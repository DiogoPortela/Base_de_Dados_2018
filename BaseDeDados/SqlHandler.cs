﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace BaseDeDados {
    static class SqlHandler {

        public static SqlConnection con;

        public static void init() {
            con = new SqlConnection("Data Source=DESKTOP-OH1T17D;Initial Catalog=projecto;Integrated Security=True"); //db local - pedro
            //             = new SqlConnection("Data Source=Chaleilei;Initial Catalog=projeto1;Integrated Security=True"); //db local - diogo
            con.Open();
        }

        public static void LoadDataGridView(string sqlCommand, DataGridView gridView) {
            try {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);

                DataTable table = new DataTable();
                adapter.Fill(table);
                gridView.DataSource = table;
                gridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            } catch {
                MessageBox.Show("Error loading data to Data Grid View.");
            }
        }

        public static void LoadComboBox(string sqlCommand, ComboBox comboBox) {
            try {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);

                DataTable table = new DataTable();
                adapter.Fill(table);

                if (comboBox.Items.Count > 0)
                    comboBox.Items.Clear();

                for (int i = 0; i < table.Rows.Count; i++)
                    comboBox.Items.Add(table.Rows[i][0].ToString());
            } catch {
                MessageBox.Show("Error loading data to combo box.");
            }

        }

        public static void SendDataTable(string sqlCommand) {
            try {
                SqlCommand command = new SqlCommand(sqlCommand, con);
                command.ExecuteNonQuery();
            } catch (Exception e) {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// ExternalInsert Stored Procedure - ExternalInsert.sql na pasta resources do projecto
        /// </summary>
        public static void InsertExternal(int zipID, int contactID, string address, string name, int fiscalNumber) {

            try {
                SqlDataAdapter da = new SqlDataAdapter("ExternalInsert", con);

                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@zip_id", SqlDbType.Int).Value = zipID;
                da.SelectCommand.Parameters.Add("@contact_id", SqlDbType.Int).Value = contactID;
                da.SelectCommand.Parameters.Add("@ext_address", SqlDbType.VarChar).Value = address;
                da.SelectCommand.Parameters.Add("@ext_name", SqlDbType.VarChar).Value = name;
                da.SelectCommand.Parameters.Add("@ext_fiscal_number", SqlDbType.Int).Value = fiscalNumber;

                da.SelectCommand.ExecuteNonQuery();

            } catch (Exception e) {
                MessageBox.Show(e.Message);
            }
        }
    }
}
