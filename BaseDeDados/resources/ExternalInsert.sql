﻿USE [projecto]
GO

CREATE PROCEDURE [dbo].[ExternalInsert]
	   @zip_id int,
	   @contact_id int,
	   @ext_address varchar(50),
	   @ext_name varchar(50),
	   @ext_fiscal_number int
AS
BEGIN
	INSERT INTO [External] (ZIP_ID, CONTACT_ID, EXT_ADDRESS, EXT_NAME, EXT_FISCAL_NUMBER)
	VALUES (@zip_id, @contact_id, @ext_address, @ext_name, @ext_fiscal_number)
END
GO