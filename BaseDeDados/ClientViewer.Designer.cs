﻿namespace BaseDeDados
{
    partial class ClientViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridView_Client = new System.Windows.Forms.DataGridView();
            this.label_name = new System.Windows.Forms.Label();
            this.combo_contact = new System.Windows.Forms.ComboBox();
            this.combo_zipcode = new System.Windows.Forms.ComboBox();
            this.text_name = new System.Windows.Forms.TextBox();
            this.label_address = new System.Windows.Forms.Label();
            this.label_fiscal_number = new System.Windows.Forms.Label();
            this.label_contact = new System.Windows.Forms.Label();
            this.label_zipcode = new System.Windows.Forms.Label();
            this.text_address = new System.Windows.Forms.TextBox();
            this.text_fiscal_number = new System.Windows.Forms.TextBox();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_update = new System.Windows.Forms.Button();
            this.button_Remove = new System.Windows.Forms.Button();
            this.button_contact = new System.Windows.Forms.Button();
            this.button_zipcode = new System.Windows.Forms.Button();
            this.gridView_Externals = new System.Windows.Forms.DataGridView();
            this.label_externals = new System.Windows.Forms.Label();
            this.label_clients = new System.Windows.Forms.Label();
            this.button_addExternal = new System.Windows.Forms.Button();
            this.button_removeExternal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Client)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Externals)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView_Client
            // 
            this.gridView_Client.AllowUserToAddRows = false;
            this.gridView_Client.AllowUserToDeleteRows = false;
            this.gridView_Client.AllowUserToResizeRows = false;
            this.gridView_Client.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridView_Client.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.gridView_Client.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridView_Client.Location = new System.Drawing.Point(340, 31);
            this.gridView_Client.Margin = new System.Windows.Forms.Padding(4);
            this.gridView_Client.Name = "gridView_Client";
            this.gridView_Client.ReadOnly = true;
            this.gridView_Client.RowHeadersVisible = false;
            this.gridView_Client.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridView_Client.Size = new System.Drawing.Size(939, 630);
            this.gridView_Client.TabIndex = 0;
            this.gridView_Client.VirtualMode = true;
            this.gridView_Client.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.clientGridView_CellClick);
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(1288, 34);
            this.label_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(49, 17);
            this.label_name.TabIndex = 1;
            this.label_name.Text = "Name:";
            // 
            // combo_contact
            // 
            this.combo_contact.FormattingEnabled = true;
            this.combo_contact.Location = new System.Drawing.Point(1388, 127);
            this.combo_contact.Margin = new System.Windows.Forms.Padding(4);
            this.combo_contact.Name = "combo_contact";
            this.combo_contact.Size = new System.Drawing.Size(160, 24);
            this.combo_contact.TabIndex = 2;
            // 
            // combo_zipcode
            // 
            this.combo_zipcode.FormattingEnabled = true;
            this.combo_zipcode.Location = new System.Drawing.Point(1388, 160);
            this.combo_zipcode.Margin = new System.Windows.Forms.Padding(4);
            this.combo_zipcode.Name = "combo_zipcode";
            this.combo_zipcode.Size = new System.Drawing.Size(160, 24);
            this.combo_zipcode.TabIndex = 3;
            // 
            // text_name
            // 
            this.text_name.Location = new System.Drawing.Point(1388, 31);
            this.text_name.Margin = new System.Windows.Forms.Padding(4);
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(199, 22);
            this.text_name.TabIndex = 4;
            // 
            // label_address
            // 
            this.label_address.AutoSize = true;
            this.label_address.Location = new System.Drawing.Point(1288, 66);
            this.label_address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(64, 17);
            this.label_address.TabIndex = 5;
            this.label_address.Text = "Address:";
            // 
            // label_fiscal_number
            // 
            this.label_fiscal_number.AutoSize = true;
            this.label_fiscal_number.Location = new System.Drawing.Point(1288, 98);
            this.label_fiscal_number.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_fiscal_number.Name = "label_fiscal_number";
            this.label_fiscal_number.Size = new System.Drawing.Size(70, 17);
            this.label_fiscal_number.TabIndex = 6;
            this.label_fiscal_number.Text = "Fiscal No:";
            // 
            // label_contact
            // 
            this.label_contact.AutoSize = true;
            this.label_contact.Location = new System.Drawing.Point(1288, 130);
            this.label_contact.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_contact.Name = "label_contact";
            this.label_contact.Size = new System.Drawing.Size(60, 17);
            this.label_contact.TabIndex = 7;
            this.label_contact.Text = "Contact:";
            // 
            // label_zipcode
            // 
            this.label_zipcode.AutoSize = true;
            this.label_zipcode.Location = new System.Drawing.Point(1292, 164);
            this.label_zipcode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_zipcode.Name = "label_zipcode";
            this.label_zipcode.Size = new System.Drawing.Size(69, 17);
            this.label_zipcode.TabIndex = 8;
            this.label_zipcode.Text = "Zip Code:";
            // 
            // text_address
            // 
            this.text_address.Location = new System.Drawing.Point(1388, 63);
            this.text_address.Margin = new System.Windows.Forms.Padding(4);
            this.text_address.Name = "text_address";
            this.text_address.Size = new System.Drawing.Size(199, 22);
            this.text_address.TabIndex = 9;
            // 
            // text_fiscal_number
            // 
            this.text_fiscal_number.Location = new System.Drawing.Point(1388, 95);
            this.text_fiscal_number.Margin = new System.Windows.Forms.Padding(4);
            this.text_fiscal_number.Name = "text_fiscal_number";
            this.text_fiscal_number.Size = new System.Drawing.Size(199, 22);
            this.text_fiscal_number.TabIndex = 10;
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(1292, 193);
            this.button_Add.Margin = new System.Windows.Forms.Padding(4);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(296, 28);
            this.button_Add.TabIndex = 11;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(1292, 229);
            this.button_update.Margin = new System.Windows.Forms.Padding(4);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(296, 28);
            this.button_update.TabIndex = 12;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // button_Remove
            // 
            this.button_Remove.Location = new System.Drawing.Point(1292, 265);
            this.button_Remove.Margin = new System.Windows.Forms.Padding(4);
            this.button_Remove.Name = "button_Remove";
            this.button_Remove.Size = new System.Drawing.Size(296, 28);
            this.button_Remove.TabIndex = 13;
            this.button_Remove.Text = "Remove";
            this.button_Remove.UseVisualStyleBackColor = true;
            this.button_Remove.Click += new System.EventHandler(this.button_Remove_Click);
            // 
            // button_contact
            // 
            this.button_contact.Location = new System.Drawing.Point(1557, 127);
            this.button_contact.Margin = new System.Windows.Forms.Padding(4);
            this.button_contact.Name = "button_contact";
            this.button_contact.Size = new System.Drawing.Size(31, 28);
            this.button_contact.TabIndex = 14;
            this.button_contact.Text = "+";
            this.button_contact.UseVisualStyleBackColor = true;
            this.button_contact.Click += new System.EventHandler(this.button_contact_Click);
            // 
            // button_zipcode
            // 
            this.button_zipcode.Location = new System.Drawing.Point(1557, 158);
            this.button_zipcode.Margin = new System.Windows.Forms.Padding(4);
            this.button_zipcode.Name = "button_zipcode";
            this.button_zipcode.Size = new System.Drawing.Size(31, 28);
            this.button_zipcode.TabIndex = 15;
            this.button_zipcode.Text = "+";
            this.button_zipcode.UseVisualStyleBackColor = true;
            this.button_zipcode.Click += new System.EventHandler(this.button_zipcode_Click);
            // 
            // gridView_Externals
            // 
            this.gridView_Externals.AllowUserToAddRows = false;
            this.gridView_Externals.AllowUserToDeleteRows = false;
            this.gridView_Externals.AllowUserToResizeRows = false;
            this.gridView_Externals.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridView_Externals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridView_Externals.Location = new System.Drawing.Point(12, 31);
            this.gridView_Externals.Margin = new System.Windows.Forms.Padding(4);
            this.gridView_Externals.Name = "gridView_Externals";
            this.gridView_Externals.ReadOnly = true;
            this.gridView_Externals.RowHeadersVisible = false;
            this.gridView_Externals.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridView_Externals.Size = new System.Drawing.Size(320, 630);
            this.gridView_Externals.TabIndex = 16;
            // 
            // label_externals
            // 
            this.label_externals.AutoSize = true;
            this.label_externals.Location = new System.Drawing.Point(133, 11);
            this.label_externals.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_externals.Name = "label_externals";
            this.label_externals.Size = new System.Drawing.Size(70, 17);
            this.label_externals.TabIndex = 17;
            this.label_externals.Text = "Externals:";
            // 
            // label_clients
            // 
            this.label_clients.AutoSize = true;
            this.label_clients.Location = new System.Drawing.Point(783, 11);
            this.label_clients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_clients.Name = "label_clients";
            this.label_clients.Size = new System.Drawing.Size(54, 17);
            this.label_clients.TabIndex = 18;
            this.label_clients.Text = "Clients:";
            // 
            // button_addExternal
            // 
            this.button_addExternal.Location = new System.Drawing.Point(1292, 300);
            this.button_addExternal.Margin = new System.Windows.Forms.Padding(4);
            this.button_addExternal.Name = "button_addExternal";
            this.button_addExternal.Size = new System.Drawing.Size(296, 28);
            this.button_addExternal.TabIndex = 19;
            this.button_addExternal.Text = "Add from External";
            this.button_addExternal.UseVisualStyleBackColor = true;
            this.button_addExternal.Click += new System.EventHandler(this.button_addExternal_Click);
            // 
            // button_removeExternal
            // 
            this.button_removeExternal.Location = new System.Drawing.Point(1292, 336);
            this.button_removeExternal.Margin = new System.Windows.Forms.Padding(4);
            this.button_removeExternal.Name = "button_removeExternal";
            this.button_removeExternal.Size = new System.Drawing.Size(296, 28);
            this.button_removeExternal.TabIndex = 20;
            this.button_removeExternal.Text = "Remove from External";
            this.button_removeExternal.UseVisualStyleBackColor = true;
            this.button_removeExternal.Click += new System.EventHandler(this.button_removeExternal_Click);
            // 
            // ClientViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1611, 679);
            this.Controls.Add(this.button_removeExternal);
            this.Controls.Add(this.button_addExternal);
            this.Controls.Add(this.label_clients);
            this.Controls.Add(this.label_externals);
            this.Controls.Add(this.gridView_Externals);
            this.Controls.Add(this.button_zipcode);
            this.Controls.Add(this.button_contact);
            this.Controls.Add(this.button_Remove);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.text_fiscal_number);
            this.Controls.Add(this.text_address);
            this.Controls.Add(this.label_zipcode);
            this.Controls.Add(this.label_contact);
            this.Controls.Add(this.label_fiscal_number);
            this.Controls.Add(this.label_address);
            this.Controls.Add(this.text_name);
            this.Controls.Add(this.combo_zipcode);
            this.Controls.Add(this.combo_contact);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.gridView_Client);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ClientViewer";
            this.Text = "ClientViewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientViewer_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Client)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Externals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridView_Client;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.ComboBox combo_contact;
        private System.Windows.Forms.ComboBox combo_zipcode;
        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.Label label_address;
        private System.Windows.Forms.Label label_fiscal_number;
        private System.Windows.Forms.Label label_contact;
        private System.Windows.Forms.Label label_zipcode;
        private System.Windows.Forms.TextBox text_address;
        private System.Windows.Forms.TextBox text_fiscal_number;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.Button button_Remove;
        private System.Windows.Forms.Button button_contact;
        private System.Windows.Forms.Button button_zipcode;
        private System.Windows.Forms.DataGridView gridView_Externals;
        private System.Windows.Forms.Label label_externals;
        private System.Windows.Forms.Label label_clients;
        private System.Windows.Forms.Button button_addExternal;
        private System.Windows.Forms.Button button_removeExternal;
    }
}