﻿using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BaseDeDados
{
    public partial class ClientViewer : Form
    {
        public ClientViewer()
        {
            CenterToScreen();
            InitializeComponent();
            UpdateData();
            UpdateComboboxes();
        }

        public void UpdateData()
        {
            SqlHandler.LoadDataGridView("Select Client.EXT_ID as ID, [External].EXT_NAME as Name, [External].EXT_ADDRESS as Address, [External].EXT_FISCAL_NUMBER as 'Fiscal Number', [External].ZIP_ID as 'Zip', [External].Contact_ID as 'Contact' From[External] join Client on[External].EXT_ID = Client.EXT_ID", this.gridView_Client);
            SqlHandler.LoadDataGridView("Select EXT_ID as ID, [External].EXT_NAME as Name From[External]", this.gridView_Externals);
        }
        public void UpdateComboboxes()
        {
            SqlHandler.LoadComboBox("SELECT ZIP_ID FROM ZIPCODE", combo_zipcode);
            SqlHandler.LoadComboBox("SELECT CONTACT_ID FROM CONTACT", combo_contact);
        }

        private void button_contact_Click(object sender, System.EventArgs e)
        {
            ContactManager cm = new ContactManager(this);
            cm.Show();
        }
        private void button_zipcode_Click(object sender, System.EventArgs e)
        {
            ZipCodeManager zcm = new ZipCodeManager(this);
            zcm.Show();
        }

        private void button_Add_Click(object sender, System.EventArgs e)
        {
            if (text_name.Text.Length > 0 && text_fiscal_number.Text.Length > 0 && text_address.Text.Length > 0 && combo_contact.SelectedItem != null && combo_zipcode.SelectedItem != null)
            {
                int fiscalNumber;
                if(int.TryParse(text_fiscal_number.Text, out fiscalNumber))
                {
                    SqlHandler.InsertExternal(int.Parse(combo_zipcode.SelectedItem.ToString()),
                                              int.Parse(combo_contact.SelectedItem.ToString()),
                                              text_address.Text,
                                              text_name.Text,
                                              fiscalNumber);
                    UpdateData();
                }
                else
                {
                    MessageBox.Show("Fiscal Number is not all numbers");
                }
            }
            else
            {
                MessageBox.Show("Cannot have empty fields");
            }
        }

        private void button_update_Click(object sender, System.EventArgs e)
        {
            if (gridView_Client.SelectedCells.Count > 0)
            {
                int oldclientId = 0;

                foreach (DataGridViewCell cell in gridView_Client.SelectedCells)
                {
                    if (cell.OwningColumn.HeaderText.ToUpper() == "ID")
                    {
                        int.TryParse(cell.Value.ToString(), out oldclientId);
                    }
                }

                if (text_name.Text.Length > 0 && text_fiscal_number.Text.Length > 0 && text_address.Text.Length > 0 && combo_contact.SelectedItem != null && combo_zipcode.SelectedItem != null)
                {
                    int fiscalNumber;
                    if (int.TryParse(text_fiscal_number.Text, out fiscalNumber))
                    {
                        SqlHandler.SendDataTable("UPDATE [External] SET EXT_NAME = '" + text_name.Text + "', EXT_ADDRESS = '" + text_address.Text + "', EXT_FISCAL_NUMBER = " + fiscalNumber + ", CONTACT_ID = " + int.Parse(combo_contact.SelectedItem.ToString()) + ", ZIP_ID = " + int.Parse(combo_zipcode.SelectedItem.ToString()) + " Where EXT_ID = " + oldclientId);
                        UpdateData();
                    }
                    else
                    {
                        MessageBox.Show("Fiscal Number is not all numberes");
                    }
                }
                else
                {
                    MessageBox.Show("Cannot have empty fields");
                }
            }
            else
            {
                MessageBox.Show("You have nothing selected");
            }
        }

        private void button_Remove_Click(object sender, System.EventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Client.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "ID")
                {
                    int clientId;
                    int.TryParse(cell.Value.ToString(), out clientId);
                    SqlHandler.SendDataTable("DELETE FROM Client WHERE EXT_ID = " + clientId);

                    UpdateData();
                }
            }
        }

        private void clientGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Client.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "NAME")
                {
                    text_name.Text = cell.Value.ToString();
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "ADDRESS")
                {
                    text_address.Text = cell.Value.ToString();
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "FISCAL NUMBER")
                {
                    text_fiscal_number.Text = cell.Value.ToString();
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "ZIP")
                {
                    combo_zipcode.SelectedIndex = combo_zipcode.Items.IndexOf(cell.Value.ToString());
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "CONTACT")
                {
                    combo_contact.SelectedIndex = combo_contact.Items.IndexOf(cell.Value.ToString());
                }
            }

        }

        private void button_addExternal_Click(object sender, System.EventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Externals.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "ID")
                {
                    int clientId;
                    int.TryParse(cell.Value.ToString(), out clientId);
                    SqlHandler.SendDataTable("INSERT INTO Client (EXT_ID) VALUES (" + clientId + ")");

                    UpdateData();
                }
            }
        }

        private void button_removeExternal_Click(object sender, System.EventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Externals.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "ID")
                {
                    int clientId;
                    int.TryParse(cell.Value.ToString(), out clientId);
                    SqlHandler.SendDataTable("DELETE FROM [External] WHERE EXT_ID = " + clientId);

                    UpdateData();
                }
            }
        }

        private void ClientViewer_FormClosing(object sender, FormClosingEventArgs e) {
            Application.Exit();
        }
    }
}