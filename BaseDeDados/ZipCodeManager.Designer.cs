﻿namespace BaseDeDados
{
    partial class ZipCodeManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridView_zip = new System.Windows.Forms.DataGridView();
            this.label_zip = new System.Windows.Forms.Label();
            this.text_zip = new System.Windows.Forms.TextBox();
            this.text_location = new System.Windows.Forms.TextBox();
            this.button_add = new System.Windows.Forms.Button();
            this.button_remove = new System.Windows.Forms.Button();
            this.label_location = new System.Windows.Forms.Label();
            this.button_update = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_zip)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView_zip
            // 
            this.gridView_zip.AllowUserToAddRows = false;
            this.gridView_zip.AllowUserToDeleteRows = false;
            this.gridView_zip.AllowUserToOrderColumns = true;
            this.gridView_zip.AllowUserToResizeRows = false;
            this.gridView_zip.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridView_zip.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridView_zip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridView_zip.Location = new System.Drawing.Point(12, 12);
            this.gridView_zip.MultiSelect = false;
            this.gridView_zip.Name = "gridView_zip";
            this.gridView_zip.ReadOnly = true;
            this.gridView_zip.RowHeadersVisible = false;
            this.gridView_zip.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridView_zip.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridView_zip.ShowCellToolTips = false;
            this.gridView_zip.ShowEditingIcon = false;
            this.gridView_zip.Size = new System.Drawing.Size(240, 162);
            this.gridView_zip.TabIndex = 0;
            this.gridView_zip.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridView_zip_CellClick);
            // 
            // label_zip
            // 
            this.label_zip.AutoSize = true;
            this.label_zip.Location = new System.Drawing.Point(256, 12);
            this.label_zip.Name = "label_zip";
            this.label_zip.Size = new System.Drawing.Size(50, 13);
            this.label_zip.TabIndex = 1;
            this.label_zip.Text = "ZipCode:";
            // 
            // text_zip
            // 
            this.text_zip.Location = new System.Drawing.Point(258, 28);
            this.text_zip.Name = "text_zip";
            this.text_zip.Size = new System.Drawing.Size(168, 20);
            this.text_zip.TabIndex = 2;
            // 
            // text_location
            // 
            this.text_location.Location = new System.Drawing.Point(258, 67);
            this.text_location.Name = "text_location";
            this.text_location.Size = new System.Drawing.Size(168, 20);
            this.text_location.TabIndex = 3;
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(257, 93);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(169, 23);
            this.button_add.TabIndex = 4;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_remove
            // 
            this.button_remove.Location = new System.Drawing.Point(257, 151);
            this.button_remove.Name = "button_remove";
            this.button_remove.Size = new System.Drawing.Size(169, 23);
            this.button_remove.TabIndex = 5;
            this.button_remove.Text = "Remove";
            this.button_remove.UseVisualStyleBackColor = true;
            this.button_remove.Click += new System.EventHandler(this.button_remove_Click);
            // 
            // label_location
            // 
            this.label_location.AutoSize = true;
            this.label_location.Location = new System.Drawing.Point(255, 51);
            this.label_location.Name = "label_location";
            this.label_location.Size = new System.Drawing.Size(51, 13);
            this.label_location.TabIndex = 6;
            this.label_location.Text = "Location:";
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(258, 122);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(168, 23);
            this.button_update.TabIndex = 7;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // ZipCodeManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 191);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.label_location);
            this.Controls.Add(this.button_remove);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.text_location);
            this.Controls.Add(this.text_zip);
            this.Controls.Add(this.label_zip);
            this.Controls.Add(this.gridView_zip);
            this.Name = "ZipCodeManager";
            this.Text = "ZipCode";
            ((System.ComponentModel.ISupportInitialize)(this.gridView_zip)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridView_zip;
        private System.Windows.Forms.Label label_zip;
        private System.Windows.Forms.TextBox text_zip;
        private System.Windows.Forms.TextBox text_location;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_remove;
        private System.Windows.Forms.Label label_location;
        private System.Windows.Forms.Button button_update;
    }
}