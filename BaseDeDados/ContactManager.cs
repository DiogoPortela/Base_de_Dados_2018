﻿using System;
using System.Windows.Forms;

namespace BaseDeDados
{
    public partial class ContactManager : Form
    {
        ClientViewer clientViewer;
        public ContactManager(ClientViewer clientViewer)
        {
            InitializeComponent();
            SqlHandler.LoadDataGridView("Select * From Contact", this.gridView_Contact);
            this.clientViewer = clientViewer;

        }

        private void button_add_Click(object sender, EventArgs e)
        {
            if (text_phone.Text.Length > 0 && text_email.Text.Length > 0)
            {
                int phone;
                if (int.TryParse(text_phone.Text, out phone))
                {
                    SqlHandler.SendDataTable("Insert into Contact (Contact_Number, Contact_Email) values (" + phone + ", '" + text_email.Text + "')");
                    SqlHandler.LoadDataGridView("Select * From Contact", this.gridView_Contact);
                    clientViewer.UpdateComboboxes();
                }
                else
                {
                    MessageBox.Show("Phone number is not all numbers");
                }
            }
            else
            {
                MessageBox.Show("Cannot have empty fields");
            }
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            if (gridView_Contact.SelectedCells.Count > 0)
            {
                int phone;
                int oldContactID = 0;

                foreach (DataGridViewCell cell in gridView_Contact.SelectedCells)
                {
                    if (cell.OwningColumn.HeaderText.ToUpper() == "CONTACT_ID")
                    {
                        int.TryParse(cell.Value.ToString(), out oldContactID);
                    }
                }

                if (text_email.Text.Length > 0 && text_phone.Text.Length > 0 && oldContactID != 0)
                {
                    if (int.TryParse(text_phone.Text, out phone))
                    {
                        SqlHandler.SendDataTable("UPDATE Contact SET CONTACT_NUMBER = " + phone + ", CONTACT_EMAIL = '" + text_email.Text + "' Where CONTACT_ID =" + oldContactID);
                        SqlHandler.LoadDataGridView("Select * From Contact", this.gridView_Contact);
                        clientViewer.UpdateComboboxes();
                    }
                    else
                    {
                        MessageBox.Show("Phone number is not all numberes");
                    }
                }
                else
                {
                    MessageBox.Show("Cannot have empty fields");
                }
            }
            else
            {
                MessageBox.Show("You have nothing selected");
            }
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Contact.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "CONTACT_ID")
                {
                    int contactId;
                    int.TryParse(cell.Value.ToString(), out contactId);
                    SqlHandler.SendDataTable("DELETE FROM Contact WHERE Contact_Id = " + contactId);
                    SqlHandler.LoadDataGridView("Select * From Contact", this.gridView_Contact);
                    clientViewer.UpdateComboboxes();
                }
            }
        }

        private void gridView_Contact_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_Contact.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "CONTACT_NUMBER")
                {
                    text_phone.Text = cell.Value.ToString();
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "CONTACT_EMAIL")
                {
                    text_email.Text = cell.Value.ToString();
                }
            }
        }
    }
}
