﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaseDeDados {

    public partial class Login : Form {

        public Login() {
            CenterToScreen();
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e) {
            if(text_user.Text.Equals("admin") && text_password.Text.Equals("admin")) {
                SqlHandler.init();
                ClientViewer cv = new ClientViewer();
                cv.Show();
                this.Hide();
            } else {
                MessageBox.Show("User e/ou Password erradas. Tente novamente.");
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e) {
            Application.Exit();
        }
    }
}
