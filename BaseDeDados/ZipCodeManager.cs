﻿using System;
using System.Windows.Forms;

namespace BaseDeDados
{
    public partial class ZipCodeManager : Form
    {
        ClientViewer clientViewer;

        public ZipCodeManager(ClientViewer clientViewer)
        {
            CenterToScreen();
            InitializeComponent();
            SqlHandler.LoadDataGridView("Select * From ZipCode", this.gridView_zip);
            this.clientViewer = clientViewer;
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            if (text_location.Text.Length > 0 && text_zip.Text.Length > 0)
            {
                int zip;
                if (int.TryParse(text_zip.Text, out zip))
                {
                    SqlHandler.SendDataTable("Insert into ZipCode (ZIP_ID, ZIP_DESC) values (" + zip + ", '" + text_location.Text + "')");
                    SqlHandler.LoadDataGridView("Select * From ZipCode", this.gridView_zip);
                    clientViewer.UpdateComboboxes();
                }
                else
                {
                    MessageBox.Show("Zip code is not all numbers");
                }
            }
            else
            {
                MessageBox.Show("Cannot have empty fields");
            }
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            if(gridView_zip.SelectedCells.Count > 0)
            {
                int zipId;
                int oldZipId = 0;
                string zipDesc = text_location.Text;

                foreach (DataGridViewCell cell in gridView_zip.SelectedCells)
                {
                    if (cell.OwningColumn.HeaderText.ToUpper() == "ZIP_ID")
                    {
                        int.TryParse(cell.Value.ToString(), out oldZipId);
                    }
                }

                if (zipDesc.Length > 0 && text_location.Text.Length > 0 && oldZipId != 0)
                {
                    if (int.TryParse(text_zip.Text, out zipId))
                    {
                        SqlHandler.SendDataTable("UPDATE ZipCode SET ZIP_ID = " + zipId + ", ZIP_DESC = '" + zipDesc + "' Where ZIP_ID =" + oldZipId);
                        SqlHandler.LoadDataGridView("Select * From ZipCode", this.gridView_zip);
                        clientViewer.UpdateComboboxes();
                    }
                    else
                    {
                        MessageBox.Show("Zip code is not all numberes");
                    }
                }
                else
                {
                    MessageBox.Show("Cannot have empty fields");
                }
            }
            else
            {
                MessageBox.Show("You have nothing selected");
            }
            
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewCell cell in gridView_zip.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "ZIP_ID")
                {
                    int zipId;
                    int.TryParse(cell.Value.ToString(), out zipId);
                    SqlHandler.SendDataTable("DELETE FROM ZipCode WHERE ZIP_ID = " + zipId);
                    SqlHandler.LoadDataGridView("Select * From ZipCode", this.gridView_zip);
                    clientViewer.UpdateComboboxes();
                }
            }
        }

        private void gridView_zip_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewCell cell in gridView_zip.SelectedCells)
            {
                if (cell.OwningColumn.HeaderText.ToUpper() == "ZIP_ID")
                {
                    text_zip.Text = cell.Value.ToString();
                }
                else if (cell.OwningColumn.HeaderText.ToUpper() == "ZIP_DESC")
                {
                    text_location.Text = cell.Value.ToString();
                }
            }
        }
    }
}