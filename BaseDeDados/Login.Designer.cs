﻿namespace BaseDeDados {
    partial class Login {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.pic_login = new System.Windows.Forms.PictureBox();
            this.label_user = new System.Windows.Forms.Label();
            this.label_password = new System.Windows.Forms.Label();
            this.text_user = new System.Windows.Forms.TextBox();
            this.text_password = new System.Windows.Forms.TextBox();
            this.btn_login = new System.Windows.Forms.Button();
            this.label_nomeProjecto = new System.Windows.Forms.Label();
            this.label_trabalhoAAD = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_login)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_login
            // 
            this.pic_login.Dock = System.Windows.Forms.DockStyle.Top;
            this.pic_login.ImageLocation = "C:\\Dev\\Base_de_Dados_2018\\BaseDeDados\\resources\\lock.png";
            this.pic_login.InitialImage = ((System.Drawing.Image)(resources.GetObject("pic_login.InitialImage")));
            this.pic_login.Location = new System.Drawing.Point(0, 0);
            this.pic_login.Name = "pic_login";
            this.pic_login.Size = new System.Drawing.Size(782, 200);
            this.pic_login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_login.TabIndex = 0;
            this.pic_login.TabStop = false;
            // 
            // label_user
            // 
            this.label_user.AutoSize = true;
            this.label_user.Location = new System.Drawing.Point(262, 269);
            this.label_user.Name = "label_user";
            this.label_user.Size = new System.Drawing.Size(73, 17);
            this.label_user.TabIndex = 99;
            this.label_user.Text = "Username";
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(262, 297);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(69, 17);
            this.label_password.TabIndex = 99;
            this.label_password.Text = "Password";
            // 
            // text_user
            // 
            this.text_user.Location = new System.Drawing.Point(348, 266);
            this.text_user.Name = "text_user";
            this.text_user.Size = new System.Drawing.Size(176, 22);
            this.text_user.TabIndex = 100;
            // 
            // text_password
            // 
            this.text_password.Location = new System.Drawing.Point(348, 294);
            this.text_password.Name = "text_password";
            this.text_password.Size = new System.Drawing.Size(176, 22);
            this.text_password.TabIndex = 101;
            this.text_password.UseSystemPasswordChar = true;
            // 
            // btn_login
            // 
            this.btn_login.Location = new System.Drawing.Point(265, 361);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(259, 31);
            this.btn_login.TabIndex = 102;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // label_nomeProjecto
            // 
            this.label_nomeProjecto.AutoSize = true;
            this.label_nomeProjecto.Location = new System.Drawing.Point(635, 527);
            this.label_nomeProjecto.Name = "label_nomeProjecto";
            this.label_nomeProjecto.Size = new System.Drawing.Size(135, 17);
            this.label_nomeProjecto.TabIndex = 103;
            this.label_nomeProjecto.Text = "Portela e Silva, Lda.";
            // 
            // label_trabalhoAAD
            // 
            this.label_trabalhoAAD.AutoSize = true;
            this.label_trabalhoAAD.Location = new System.Drawing.Point(12, 527);
            this.label_trabalhoAAD.Name = "label_trabalhoAAD";
            this.label_trabalhoAAD.Size = new System.Drawing.Size(306, 17);
            this.label_trabalhoAAD.TabIndex = 104;
            this.label_trabalhoAAD.Text = "Projecto de Armazenamento e Acesso a Dados";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 553);
            this.Controls.Add(this.label_trabalhoAAD);
            this.Controls.Add(this.label_nomeProjecto);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.text_password);
            this.Controls.Add(this.text_user);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_user);
            this.Controls.Add(this.pic_login);
            this.Name = "Login";
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pic_login)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_login;
        private System.Windows.Forms.Label label_user;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.TextBox text_user;
        private System.Windows.Forms.TextBox text_password;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label label_nomeProjecto;
        private System.Windows.Forms.Label label_trabalhoAAD;
    }
}