/* qual e o stock actual de sementes de 'oliveira' em armazem? */
SELECT WH_ITEM_QUANTITY AS 'Stock sementes de Oliveira'
FROM ItemsPerWarehouse JOIN Seed ON ItemsPerWarehouse.ITEM_ID = Seed.ITEM_ID
					   JOIN SeedType ON Seed.SEED_TYPE_ID = SeedType.SEED_TYPE_ID
WHERE SEED_DESC = 'oliveira';

/* qual e o tipo de semente mais requisitado pelos clientes da empresa */
SELECT SeedType.SEED_DESC AS 'Seed Requested', COUNT(ItemsPerTransactiON.TRANS_ITEM_QUANTITY) AS 'Quantity'
FROM SeedType JOIN Seed ON SeedType.SEED_TYPE_ID = Seed.SEED_TYPE_ID
			  JOIN ItemsPerTransactiON ON Seed.ITEM_ID = ItemsPerTransactiON.ITEM_ID
			  JOIN [TransactiON] ON ItemsPerTransactiON.TRANS_ID = [TransactiON].TRANS_ID
			  JOIN Client ON [TransactiON].EXT_ID = Client.EXT_ID
GROUP BY SeedType.SEED_DESC
HAVING  COUNT(ItemsPerTransactiON.TRANS_ITEM_QUANTITY) >= ALL (
														  SELECT COUNT(ItemsPerTransactiON.TRANS_ITEM_QUANTITY)
														  FROM ItemsPerTransactiON JOIN Seed ON ItemsPerTransactiON.ITEM_ID = Seed.ITEM_ID
																				   JOIN SeedType ON Seed.SEED_TYPE_ID = SeedType.SEED_TYPE_ID
														  GROUP BY SeedType.SEED_TYPE_ID);

/* quais as culturas que estao presentes na estufa com o id 17 */
SELECT SeedType.SEED_DESC
FROM SeedType JOIN Culture ON SeedType.SEED_TYPE_ID = Culture.SEED_TYPE_ID
			  JOIN GreenHouse ON Culture.INF_ID = GreenHouse.INF_ID
WHERE GreenHouse.INF_ID = '17';

/* quais sao os fornecedores com menos influencia no stock de sementes da empresa? */
SELECT [External].EXT_NAME
FROM [External] JOIN Supplier ON [External].EXT_ID = Supplier.EXT_ID
				JOIN [TransactiON] ON [External].EXT_ID = [TransactiON].EXT_ID
				JOIN ItemsPerTransactiON ON [TransactiON].TRANS_ID = ItemsPerTransactiON.TRANS_ID
GROUP BY [External].EXT_NAME
HAVING COUNT(ItemsPerTransactiON.TRANS_ITEM_QUANTITY) <= ALL(	SELECT COUNT(ItemsPerTransactiON.TRANS_ITEM_QUANTITY)
																FROM ItemsPerTransactiON	JOIN [TransactiON] ON ItemsPerTransactiON.TRANS_ID = [TransactiON].TRANS_ID
																							JOIN Supplier ON [TransactiON].EXT_ID = Supplier.EXT_ID
																 GROUP BY Supplier.EXT_ID);

/* qual é a média final da média das sementes carregadas por cada empregado anualmente? */ 
	SELECT Worker_Name, AVG(WorkerAnualAverage) AS WorkerAnualAvg
	FROM
		(SELECT Worker_Name, YEAR(Item_Mov_Date) AS Year, AVG(Mov_Item_Quantity) AS WorkerAnualAverage
		FROM Worker JOIN ItemMovement ON Worker.Worker_ID = ItemMovement.Worker_ID
					JOIN ItemPerMovement ON ItemMovement.Item_Mov_ID = ItemPerMovement.Item_Mov_ID
		GROUP BY Worker_Name, YEAR(Item_Mov_Date)) AS WorkerAverages
	GROUP BY Worker_Name;

/* quais os trabalhadores que fizeram mais de 1000 manutencoes?*/
	SELECT Worker_Name, COUNT(Report.Worker_ID)
	FROM Worker JOIN Report ON Worker.Worker_ID = Report.Worker_ID
	GROUP BY Worker_name
	HAVING COUNT(Report.WORKER_ID) >= 1000;

/* quais as plantas mais rentaveis nos ultimos 5 anos de vendas à empresa 'Olindo e Filhos, Lda.'? */
	SELECT QuantityPerPlant.SEED_DESC, QuantityOfSeed * Item.ITEM_VALUE AS Income
	FROM 
		(SELECT SEED_DESC, COUNT(Req_Plant_Quantity) AS QuantityOfSeed
		 FROM SeedType	JOIN Seed ON SeedType.SEED_TYPE_ID = Seed.SEED_TYPE_ID
		 				JOIN Item ON Seed.ITEM_ID = Item.ITEM_ID
		 				JOIN SellablePlant ON Item.ITEM_ID = SellablePlant.ITEM_ID
		 				JOIN PlantPerRequest ON SellablePlant.Plant_ID = PlantPerRequest.Plant_ID
		 				JOIN Request ON PlantPerRequest.EXT_ID = Request.EXT_ID
		 				JOIN [External] ON Request.EXT_ID = [External].EXT_ID
		 WHERE YEAR(Request.REQ_DATE) >= (YEAR(GETDATE()) - 5) and [External].EXT_NAME = 'Olindo e Filhos, Lda.'
		 GROUP BY SEED_DESC) AS QuantityPerPlant
		 JOIN SeedType ON QuantityPerPlant.SEED_DESC = SeedType.SEED_DESC
		 JOIN Seed ON SeedType.SEED_TYPE_ID = Seed.SEED_TYPE_ID
		 JOIN Item ON Seed.ITEM_ID = Item.ITEM_ID
	WHERE QuantityOfSeed * Item.ITEM_VALUE >= all(	SELECT QuantityOfSeed * Item.ITEM_VALUE AS Income
													FROM 
														(SELECT SEED_DESC, COUNT(Req_Plant_Quantity) AS QuantityOfSeed
														 FROM SeedType	JOIN Seed ON SeedType.SEED_TYPE_ID = Seed.SEED_TYPE_ID
																		JOIN Item ON Seed.ITEM_ID = Item.ITEM_ID
																		JOIN SellablePlant ON Item.ITEM_ID = SellablePlant.ITEM_ID
																		JOIN PlantPerRequest ON SellablePlant.Plant_ID = PlantPerRequest.Plant_ID
																		JOIN Request ON PlantPerRequest.EXT_ID = Request.EXT_ID
																		JOIN [External] ON Request.EXT_ID = [External].EXT_ID
														 WHERE YEAR(Request.REQ_DATE) >= (YEAR(GETDATE()) - 5) and [External].EXT_NAME = 'Olindo e Filhos, Lda.'
														 GROUP BY SEED_DESC) AS QuantityPerPlant
													JOIN SeedType ON QuantityPerPlant.SEED_DESC = SeedType.SEED_DESC
													JOIN Seed ON SeedType.SEED_TYPE_ID = Seed.SEED_TYPE_ID
													JOIN Item ON Seed.ITEM_ID = Item.ITEM_ID);

/* qual a percentagem de residuos organicos que se transformam em materia organica util? */
	SELECT
		(SELECT SUM(OrganicResidue.ORG_RESIDUE_QUANTITY)
		 FROM OrganicResidue) * 100
	/
		NULLIF((SELECT SUM(ItemsPerWarehouse.WH_ITEM_QUANTITY) -- NULLIF 0 para evitar erros por nao haver dados no divisor
		 FROM ItemsPerWarehouse JOIN Item ON ItemsPerWarehouse.ITEM_ID = Item.ITEM_ID
								JOIN OrganicMatter ON Item.ITEM_ID = OrganicMatter.ITEM_ID), 0)
	AS 'Materia Organica Util';

/* qual o lucro total obtido pela empresa no ano 2005? */
	SELECT
		(SELECT COUNT(Trans_Total)
		 FROM [Transaction] JOIN Client ON [Transaction].EXT_ID = Client.EXT_ID
		 WHERE  YEAR(Trans_Date) = 2005)
	-	
		(SELECT COUNT(Trans_Total)
		 FROM [Transaction] JOIN Supplier ON [Transaction].EXT_ID = Supplier.EXT_ID
		 WHERE  YEAR(Trans_Date) = 2005)
	AS Profit;
	
/* qual a diferenca em numero dos contratos de trabalho assinados este ano e no mesmo periodo do ano passado */
	SELECT 
		(SELECT COUNT(Worker_Employment_Date)
		 FROM Worker
		 WHERE Worker_Employment_Date BETWEEN DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) AND GETDATE()
	-
		(SELECT COUNT(Worker_Employment_Date)
		 FROM Worker
		 WHERE Worker_Employment_Date BETWEEN DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0) AND DATEADD(YEAR, -1, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0))))
	AS 'Signed Contracts In One Year Time';